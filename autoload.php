<?php
/**
* Подключаем функцию автозагрузки
* APP\Models\User => ./App/Models/Users.php
*/
function __autoload($class){
	require __DIR__ . '/' . str_replace ('\\', '/', $class) . '.php';
}